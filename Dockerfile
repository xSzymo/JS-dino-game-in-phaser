#FROM docker
#RUN docker pull trinitronx/python-simplehttpserver

FROM trinitronx/python-simplehttpserver
RUN mkdir /home/app
WORKDIR /home/app

COPY index.html /home/app/index.html
COPY assets /home/app/assets
CMD python -m SimpleHTTPServer 8080
